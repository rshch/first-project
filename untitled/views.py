from django.template import base, context
from django.views.generic import TemplateView


class ViewHome(TemplateView):
    template_name = 'base.html'


class ViewHome2(TemplateView):
    template_name = 'page2.html'
